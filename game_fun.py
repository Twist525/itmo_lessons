"""
Unhappy-farmer
"""
import math
from random import randrange, choice
from time import sleep


def random_bool():
    """Возвращает случайное логическое значение"""
    return bool(randrange(2))

class Farm(object):
    """Ферма"""

    def __init__(self, animals_count=10, resources_count=10):
        self.animals_count = animals_count   
        self.resources_count = resources_count     

        self.farmer = None
        self.animals = []
        self.resources = 0

    def get_farmer(self):
        """Возвращает объект фермера"""
        return self.farmer

    def set_farmer(self, farmer):
        """Присваивает фермера ферме"""
        self.farmer = farmer

    def add_resources(self, value):
        """Добавляет ресурсы в холодильник"""
        count = self.resources_count
        total = self.resources + value
        
        if total > count:
            diff = total - count
            print(f'Слишком мало места в холодильнике, {diff} ед. выброшено и сгнило.')
            total -= diff

        self.resources = total

    def add_animal(self, animal):
        """Добавляет животное на ферму"""
        if len(self.animals) >= self.animals_count:
            print('Ферма слишком маленькая что бы развести столько скота.')
        else:
            self.animals.append(animal)
            animal.set_farm(self)

    def remove_animal(self, animal):
        """Удаляет животное с фермы"""
        if animal in self.animals:
            self.animals.remove(animal)

    def take_resources(self, value):
        """Возвращает указанное количество ресурсов из холодильника"""
        if value > self.resources:
            print('Упс, в холодильнике меньше еды чем ожидалось.')
            value = self.resources

        self.resources -= value

        return value

    def get_resources(self):
        """Забирает все ресурсы"""
        return self.resources

    def get_animals(self):
        """Возвращает животных на ферме"""
        return self.animals


class Farmer(object):
    """Фермер"""
    def __init__(self, farm, max_health):
        self.farm = farm        
        self.max_health = self.health = max_health

    def get_health(self):
        """Возвращает текущее здоровье фермера"""
        return self.health

    def is_dead(self):
        """Возвращаем истинное значение если фермер умер."""
        return self.get_health() <= 0       

    def is_hungry(self):
        """Возвращает истинное значение, если фермер голоден"""
        return self.get_health() < self.max_health  

    def get_farm(self):
        """Возвращает объкты фермы, где живет фермер"""
        return self.farm

    def eat(self):
        """Покушает"""
        farm = self.get_farm()
        # 1 ресурс = 10% здоровья
        needed = math.ceil((self.max_health - self.get_health()) / 10)
        received = farm.take_resources(needed) * 10
        self.restore_health(received)


    def attack(self, wolf):
        """Атакует волка"""
        if random_bool():
            self.reduce_health(wolf.get_health() // 5)
            wolf.kill()
            print(f'Фермер убил волка, здоровье фермера = {self.get_health()}.')
        else:
            self.reduce_health(wolf.get_health() // 10)
            print(f'Волк покусал фермера и здоровье фермера ухудшилось: {self.get_health()}.')
            

    def reduce_health(self, value):
        """Уменьшает энергию на указанное количество"""
        self.health -= value

    def feed_animals(self):
        """Кормит домашний скот"""
        for animal in self.get_farm().get_animals():
            animal.feed()
            self.reduce_health(5)
            print(f'Фермер покормил животное, здоровье животного {animal.get_health()}.') 
        print(f'Фермер накормил всех животных, теперь его здоровье {self.get_health()}.')   

    def kill_animal(self):
        """Убивает самое худое животное на ферме."""
        farm = self.get_farm()
        
        animals = sorted(farm.get_animals(), key=lambda a: a.get_weight())
        
        if not animals:
            print('На ферме больше не осталось скота.')
            return 

        animal = animals[0]
        weight = animal.get_weight()
        animal.kill()

        farm.add_resources(weight)        
    
        print(f'Фермер убил животное получил {weight} ресурсов.') 
        

    def take_resources(self):
        """Собирает и возвращает все ресурсы с фермы"""
        farm = self.get_farm()

        received = sum([a.take_resources() for a in farm.get_animals()])
        farm.add_resources(received)
        self.reduce_health(len(farm.get_animals()))
        
        return received

    def restore_health(self, value):
        '''Пополняет здоровье на указанное количество'''
        self.health += value

    def sleep(self, hours):        
        """Пополняет здоровье за счет сна"""
        base = self.max_health // 2 #100% - 8ч
        value = (hours * base // 8)
        self.restore_health(value)
        print(f'Фермер проспал {hours} часов и его здоровье {self.get_health()}.')        


class Animal(object):
    """Животное"""
    def __init__(self, health, weight, resources_count=1):
        self.health = health
        self.weight = weight
        self.resources_count = resources_count

    def get_health(self):
        """Возвращает текущие здоровье животного"""
        return self.health

    def get_weight(self):
        """Получить ресурсы от животного"""
        return self.weight

    def is_dead(self):
        """Возвращает истинное значение, если животное умерло"""
        return self.get_health() <= 0

    def take_resources(self):
        """Получить ресурсы от животного"""
        return self.resources_count

    def feed(self):
        """Покормить животное"""
        self.health += 10

    def hurt(self, value):
        """Ранить животное"""
        self.health -= value

    def kill(self):
        """Убить животное"""
        self.health = 0


class Wolf(Animal):
    """Волк"""
    def attack(self, animal):
        """Нападает на домашний скот"""
        if random_bool():
            print('Волк убил животное.')
            animal.kill()
        else:
            value = randrange(animal.get_health())
            print(f'Волк ранил животное на {value} единиц здоровья.')            
            animal.hurt(value)


class Sheep(Animal):
    """Овцы"""
    def __init__(self, health, weight, resources_count=1):
        super().__init__(health, weight, resources_count)
        self.farm = None

    def kill(self):
        super().kill()
        self.farm.remove_animal(self)

    def set_farm(self, farm):
        self.farm = farm

class Game(object):
    """Игра, игровой процесс"""
    def __init__(self):
        self.farm = None

    def init_game(self):
        """Инициализирует базовые объекты игры."""
        self.farm = farm = Farm()
        
        farmer = Farmer(farm, 100)
        farm.set_farmer(farmer)

        for _ in range(farm.animals_count):
            animal = Sheep(
                health=randrange(50, 101),
                weight=randrange(2, 41)
            )
            farm.add_animal(animal)        

    def __attack_wolves(self, day_number):
        """Атаковать ферму волками"""
        farm = self.farm
        farmer = farm.get_farmer()
        wolves_count = randrange((day_number % 7 or 7) + 1)
        wolves = [Wolf(health=randrange(10, 100), weight=randrange(1, 10)) 
                  for _ in range(wolves_count)]  
        
        print(f'К ферме подошли {wolves_count} волков, они ищут добычу.')
        
        if not wolves:
            farmer.sleep(randrange(4, 9))

        for wolf in wolves:
            farmer.attack(wolf)
            animals = farm.get_animals()

            if not wolf.is_dead() and animals:
                animal = choice(animals)
                wolf.attack(animal)

            sleep(.5)

    def start(self):
        """Начинает игру."""
        farm = self.farm
        farmer = farm.get_farmer()
        day_number = 1

        while not farmer.is_dead():
            print('__________________')
            print(f'День №{day_number}')

            self.__attack_wolves(day_number)
            
            farmer.eat()
            print(f'Фермер позавтракал, его здоровье {farmer.get_health()}.')

            received = farmer.take_resources()
            print(f'Фермер собрал с фермы {received} ресурсов.')

            #поспать в обед

            farmer.feed_animals()

            print(f'Фермер поужинал, его здоровье {farmer.get_health()}.')

            while farmer.is_hungry() and farm.get_animals():
                print(f'Фермеру не хватает ресурсов, чтобы наесться, он начинат убивать скот.')
                farmer.kill_animal()
                farmer.eat()

            day_number += 1

            sleep(.1)

    def stop(self):
        """Останавливает игру."""
        self.farm = None

if __name__ == '__main__':
    game = Game()
    game.init_game()
    game.start()
        



