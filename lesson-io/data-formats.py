"""
Формата данных
"""

data = {
    'users': [
        {
            'id': 1,
            'name': 'Linus Torvalds',
            'skills':('C++', 'Linux'),
            'is_student': False,
        },
        {
            'id': 2,
            'name': 'Richard stallman',
            'skills':('GNU', 'C', 'C++'),
            'is_student' : False,
        },
    ],
}



# todo: Pickle

import pickle

with open('users.pickle', 'wb') as f:
    pickle.dump(data, f)


with open('users.pickle', 'rb') as f:
    loaded_data = pickle.load(f)
    print('Прочитано из Pickle: ' + str(loaded_data))


# JSON (Java Script Object Notation)

import json

with open('users.json', 'w') as f:
    json.dump(data, f, indent=4)

with open('users.json') as f:
    loaded_data = json.load(f)
    print('Прочитанно из json: ' + str(loaded_data))


# todo: CSV

"""
todO:
id;name;skills;is_student
1;linus torvalds;C++,Linux;1
2;Richard Stallman;Gnu,C;1

"""

import csv

with open('users.csv','w') as f:
    users = data.get('users', [])

    if users:
        fieldnames = users[0].keys()
        writer = csv.DictWriter(f, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerows(users)

print('\n')
with open('users.csv') as f:
    reader = csv.DictReader(f)
    users = list(reader)
    print('Прочитано из CSV: ' + str(users)) 


"""
todo: INI - модуль configparser

debug 1

db.host localhost
db.user root

[db]
host localhost
user root

"""

"""
todo: XML - модуль lxml

<users> 
    <user>
        <id>1</id>
        <name>Linus Torvald</name>
        <skills>
            <skills>C++</skills>
            <skills>Linux</skills>               
        </skills>
    </user>
    <user id='1' name='Linus Torvald'> 
        <skills>
            <skills name='C++' />
            <skills>Linux</skills>               
        </skills>
    </user>    
</users>

"""






















