'''
todo: Базы данных 

SQLite3 - база данных в файле

Алгоритмы взаимодействия с БД:
1. Установить соединения с сервером бд
    1.1. Опционально (для СУБД), выбрать базу данных

2. Выполнение запросов:
    2.1. получить объект курсора (опционально) 
    2.2. выполнить запрос с помощью метода execute() объекта курсора
    2.3. если запрос на изменение структуры БД или данных:
        2.3.1. Нужно зафиксировать изменения (опционально)
    2.4. Если запрос на получения данных (SELECT):
        2.4.1. Фактические данные нужно раз-fetch-ить
            - fetchall()   - получить все строки таблицы в список
            - fetchone()   - получить одну строку таблицы 
            - fetchmany(N) - получить N строк из таблицы в список 

connect(detect_types)
Какие значения допустимы для detect_types:
PARSE_DECLTYPES анализ объявленный тип для каждого объявленного столбца
PARSE_COLNAMES   
    id [int] INTEGER 

'''

import sqlite3

sql = '''
    CREATE TABLE IF NOT EXISTS task(
        id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
        title TEXT NOT NULL, 
        description TEXT NOT NULL DEFAULT '',
        planned TIMESTAMP NOT NULL,
        done BOOLEAN NOT NULL DEFAULT 0,
        created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
    )
'''

sql_insert = '''
    INSERT INTO task (title, planned, description) VALUES (?, ?, ?)
'''

sql_update = '''
    UPDATE task SET title=?, planned=?, description=?

    WHERE id=?
'''

sql_delete = '''
    DELETE FROM task WHERE id=?
'''

sql_select = '''
    SELECT 
        id, title, planned, description, done, created
    FROM 
        task
'''

#conn = sqlite3.connect('db.sqlite')
#cursor = conn.cursor()
#cursor.execute(sql)
#conn.commit()
#conn.close()

with sqlite3.connect('db.sqlite', detect_types=sqlite3.PARSE_DECLTYPES) as conn:
    conn.row_factory = sqlite3.Row    

    conn.execute(sql)

    conn.execute(
        sql_insert, ('Сделать ДЗ', '2019-10-10 00:00:00', 'Срочно!'))

    cursor = conn.execute(sql_select)
    tasks = cursor.fetchall()

    print(tasks)

    for task in tasks:
        print(task["title"] + ' ' + str(task["planned"]))

























