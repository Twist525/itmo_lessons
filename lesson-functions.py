"""
todo: Функции 
"""

def say_hello(word):# Объявить функцию
    print(word)


say_hello('Hello!')# Вызвать функцию

def get_hello():
    return 'Hello!!!'


result = get_hello()
print(result)

def print_greeting(username):# Как передать в функци.. аргументы
    print('Hello ' + username, '!', sep='')


print_greeting(input('Введите имя! '))

def multi(a, b):
    return a * b


c = multi(5, 8)
print(c)

"""
todo: Как задать значение аргумента по умолчанию
"""

def my_pow(x, p=2) -> 'int':
    return x ** p


print(my_pow(6))
print(my_pow(5, 4))


"""
todo: Позиционные и именованные аргументы
"""

print(my_pow(p=5, x=7))
print(my_pow(6, p=8))


"""
todo: Переменное количество аргументов
"""

def multi_cool(a, b, *numbers) -> 'Any Numbers': # *numbers кучка позиционных аргументы
    print(a, b, type(numbers), numbers)
    
    results = a * b

    for i in numbers:
        results *= i

    return results


print(multi_cool(1, 2))
print(multi_cool(1, 2, 3))


def db_connect(provider, **options):# **options кучка именнованых аргументов
    print('Connecting to datebase: ', provider)
    print('===>', type(options), options)

db_connect('sqllate', filename='db.sqlite')
db_connect('mysql', host='localhost', user='root', port=666)


"""
todo: Как развернуть кортеж, список в значения
        позиционных аргументов?
"""

lst = [8, 20]
print(multi(*lst))

lst = range(1, 11)
print(multi_cool(*lst))


"""
todo: Как развернуть словарь в значения
        позиционных аргументов?
"""

config = {
    'provider': 'mysql',
    'host': '127.0.0.1',
    'user':'root',
    'pwsd':'toor',
}

db_connect(**config)

"""
todo: Явное различие аргументов в python 3
"""

def demo_arguments(a, b=None, *c, d, e=None, **f):
    print(a, b, c, d, e, f)


demo_arguments(1, d=2)
demo_arguments(1, 2, d=3, e=4)
demo_arguments(1, 2, 3, 4, 5, d=10, e=20, key=30)


"""
todo: Передача значений аргумента по ссылке 
"""

def split_pieces(s, size, output=None):
    if output is None:
        ouput = []

    start = 0
    end = len(s)

    while start < end:
        output.append(s[start:start+size])
        start += size

    return output


s = 'У вало 5 яблок, а у Пети 2, что это?'
found = []
#split_pieces(s, 4, found)
#print(found)

#found_2 = split_pieces(s, 4)
#print(found_2)


def demo_defaults(x, lst=None):
    lst = lst or []
    print(lst)    
    lst.append(x ** 2)
    print(lst)

demo_defaults(2)
demo_defaults(3)


"""
todo: Анонимные функции
callback - функция обратного вызова
"""

sqrt = lambda x: x ** .5
print(sqrt(9))


"""
todo: Рекурсивная функция
Прямая
Косвенная:
    def a():
        b()

    def b():
        a()

<если истина> if <условие> else <если ложь>
"""

def factorial(n):
    return 1 if n == 0 else n * factorial(n - 1)


"""
todo: Замыкание
"""

# Пример функции каррирования
def trim(chars=None):
    # замкнутая область видимости
    print (chars)

    def f(s):
        return s.strip(chars)

    return f


trim_spaces = trim()
trim_slash = trim('/|\\')
print(trim_spaces, trim_spaces('      123      '))
print(trim_slash, trim_slash('////////post\\\\\|||'))

"""
todo: Область видимости
Глобальная область видимости
Локальная область видимости
"""

g = 666

def f():
    external = 777

    def func():
        global g # использовать плохо
        nonlocal external         

        g += 1
        external += 1

    func()
    print(g, external)


f()






















