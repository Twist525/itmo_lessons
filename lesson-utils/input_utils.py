"""
DRY don't repeat yourself
"""

__all__ = (
    'user_input', 'input_int', 'input_float', 'input_bin' ,
    'confirm' ,'multi_line_input',    
)

def user_input(msg='Введите значение... ', default=None, value_callback=None,
                trim_spaces=True, show_default=True, required=False):
    if show_default and default is not None:
        msg += '[{}]'.format(default)

    if default is not None:
        required = False

    while 1:
        value = input('[{}]'.format(msg))#input(f'{msg}: ')

        if trim_spaces:
            value = value.strip()

        if value:
            if value_callback is None:
                return value
            try:
                return value_callback(value)
            except ValueError as err:
                print(err)
        else:
            if not required:
                return default

            print('Требуется ввести значение!')


def input_int(msg='Enter number', default=None, required=False):
    return user_input(msg, default, value_callback=int, required=required)
   

def input_float(msg='Enter number', default=None, required=False):
    return user_input(msg, default, value_callback=float, required=requered)
 

def input_bin(msg='Enter number', default=None, required=False):
    """
    def to_bin(v):
        return int(v, 2) 

    user_input(msg, default, 
               value_callback=to_bin, 
               required=requered
    )
    """
    return user_input(msg, default, 
                       value_callback=lambda v: int(v, 2), 
                       required=requered
    )


def confirm(msg='Подтвердите действие', default_yes=False, default_no=False,
            required=False):
    
    def callback(value):
#для сопастовления двух ключей используется словарь всегда
        answers = {
            'y': True,
            'yes': True,
            'n': False,
            'no': False,
        }

        answer = answers.get(value.lower())

        if answer is None:
            valid_values = '/'.join(answers.keys)
            raise ValueError('[{}]'.format(valid_values))#(f'Допустимые значения: {valid_values}')

        return answer

    if default_yes and default_no:
        raise RunTimeError('Оба аргумента default_yes и default_no заданы как True.')
    
    if default_yes:
        default = True
        msg += ' [Y/n]'
    elif default_no:
        default = False
        msg += ' [y/N]'
    else:
        default = None
        msg += ' [y/n]'

    return user_input(msg, default, value_callback=callback, 
                      show_default=False, required=required)


def multi_line_input(msg='введите текст', default=None):
    print('[{}]'.format(msg))
    print('Ctrl+D/Ctrl+Z (Windows) для завершения ввода')

    if default is not None:
        print('[оставьте поле пустым, что бы использовать значение по умолчанию]')

    text = []

    while 1:
        try:
            value = input('> ')

            if not text and not value:
                return default

            text.append(value)

        except EOFError:
            print()
            return '\n'.join(text)

print(__name__) #имя модуля не полное

if __name__ == '__main__':
    print(multi_line_input())
    #print(confirm(default_yes=True))















