from textwrap import dedent
import sys

from . import storage
from .services import make_connection


def action_lisk_tasks():
    """ Вывести список задач """


def action_add_taks():
    """ Добавить задачу """
    title = input('Название ')
    planned = input('Запланировано ')
    description = input('Описание ')
    
    with make_connection() as conn:
        task = storage.create_task(conn, title, planned, description)
    
    print('OK')
#    print(f'''Задача "{task['title']}" успешно создана.''')    


def action_edit_task():
    """ Редактировать задачу """
    task_id = input('Введите ID задачи')

    with make_connection() as conn:
        task = storage.get_task(conn, task_id)
    
        if task is None:
            print('Не найдено')
#            print(f'Задача с "{task_id}" не найдена.')
            return

        title = input('Название ')
        planned = input('Запланировано ')
        description = input('Описание ')
        storage.update_task(conn, task_id, title, planned, description)

        print('OK')
#        print(f'''Задача "{title}" успешно отредактированна.''')

def action_done_task():
    """ Завершить задачу """


def action_reopen_task():
    """ Начать задачу занова """


def action_show_menu():
    """ Show menu"""
    print(dedent('''
        1. Вывести список задач 
        2. Добавить задачу
        3. Редактировать задачу
        4. Завершить задачу
        5. Начать задачу занова
        m. Показать меню
        q. Выйти
    '''))


def action_exit():
    """ Exit """
    sys.exit(0)


actions = {
    '1': action_lisk_tasks,
    '2': action_add_taks,
    '3': action_edit_task,
    '4': action_done_task,
    '5': action_reopen_task,
    'm': action_show_menu,
    'q': action_exit,    
}


def main():
    with make_connection() as conn:
        storage.initialize(conn, 'schema.sql')        

    action_show_menu()

    while 1:
        cmd = input('\nВведите команду: ')
        action = actions.get(cmd)

        if action:
            action()
        else:
            print('Не корректная команда')

          




















