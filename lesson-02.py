"""
todo: Ветвление:  
"""

a = int(input('Enter your text: '))
b = int(input('Enter your text: '))

if a > b:
    print('a > b')

    if a is None:
        pass 
elif a == b:
    print('a = b')
else:
    print('a < b')
    

"""
todo: Циклы
break    - мгновенно прервать работу
continue - пропустить текущую итерацию !!!
"""

i = 1

while i <= -1:
    i+=1

print(i)


j = 1

while True:
    if j >= 5:
        break
    j += 1


for i in range(10):
    print(i)
    #input('Enter to continue')

d = {
    'name':'Vasay',
    'lastname':'Pupkin',
    'age': 1000
}

for k,v in d.items():
    print(k, " + ", v)


person = ('Vasya', 'Pupkin', 1000)

for i, v in enumerate(person):
    print(i, v )

print(person[1])


"""
todo: Как не нужно генерировать строки!!! 
"""

s = ''

for i in range(5):
    s += str(i)

print(s)


"""
todo: Как нужно генерировать строки!!! 
"""

s = []

for i in range(5):
    s.append(str(i))

s = ''.join(s) #список в строку, все элементы должны быть строками

print(type(s))
